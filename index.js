// console.log("Hello World!");

// ARITHMETIC OPERATIONS

let x = 1397;
let y = 7831;

// let sum = 1397 + 7831;
let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

// modulus (%)
// get the remainder from 2 divided values.

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Basic Assignment
let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// shorthand method for assignment operator
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);


assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);


assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

//Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Incrementation vs Decrementation
// Incrementation (++)
// Decrementation (--)

let z = 1;

//  ++z added 1 to its original value.
let increment = ++z;
console.log("Result of pre-incrementation" + increment);
console.log("Result of pre-incrementation" + z);

increment = z++;
console.log("Result of post-incrementation" + increment);
console.log("Result of post-incrementation" + z);

let decrement = --z;
console.log("Result of pre-decrementation" + decrement);
console.log("Result of pre-decrementation" + z);

decrement = z--;
console.log("Result of post-decrementation" + decrement);
console.log("Result of post-decrementation" + z);

// Type Coercion

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// false = 0
let numE = false + 1;
console.log(numE);

let numF = true + 1;
console.log(numF);

// Comparison Operators

let juan = "juan";

// Equality Operator (==)
// Checks 2 operands if they are equal/have the same content
// May return boolean value

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log("juan" == "juan");
console.log("juan" == juan);

// Inequality Operator (!=)
// ! = not=

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log("juan" != "juan");
console.log("juan" != juan);
console.log(0 != false);

// Strict Equality Operator (===)
// 
console.log(1 === 1);
console.log(1 === 2);
console.log("juan" === juan);

// Strict Inequality (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log("juan" !== juan);

// Relational Operator

let a = 50;
let b = 65;

// 
let  isGreaterThan = a > b;
console.log(isGreaterThan);

let  isLessThan = a < b;
console.log(isLessThan);

let  isGTorEqual = a >= b;
console.log(isGTorEqual);

let  isLTorEqual = a <= b;
console.log(isLTorEqual);

let numStr = "30";
console.log(a > numStr);

let str = "twenty";
console.log(b >= str);
// In some events, we can receive NaN
// NaN == Not a Number

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Ampersands)

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical OR Operator (|| - Double Pipe)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical AND operator: " + someRequirementsMet);

// Logical NOT Operator (! - Exclamation Point)
// Returns Opposite Value

let somesRequirementsMet = !isRegistered;
console.log("Result of Logical not Operator:" + somesRequirementsMet);






